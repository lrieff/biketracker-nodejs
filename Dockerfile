# Gets the alpine package, and installs nodejs/npm/yarn
FROM alpine:latest
RUN apk add --no-cache openssl nodejs npm yarn

# Creates the working directory, and copies the source code
WORKDIR /ctx
COPY . /ctx

# Installs the packages, installs typescript globally and compiles
RUN yarn
RUN npm install -g typescript
RUN tsc

# Exposes the ports
EXPOSE 8800
EXPOSE 8801

# Makes the docker image runnable
ENTRYPOINT [ "node" ]
CMD [ "dist/main.js" ]