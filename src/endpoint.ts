import { rejects } from 'assert';
import { IncomingMessage } from 'http';
import https from 'https';

import { CBXPKT } from './models/cbxpkt';

export class Endpoint {
  public static checkWhitelist = (ip: string): Promise<boolean> => {
    return new Promise<boolean>((resolve, reject) => {
			// Checks if the whitelist is disabled in the environment variables, this may be
			//  used in development, if so just return true
			if (process.env.DEBUG === 'Y') return resolve(true);

			// Makes sure that an IPv4 address stays in IPv4 format, since IPv6 can also display IPv4
			//  but than with an ::ffff: in front of it, which will make the whitelisting have duplicates.
			if (ip.substr(0, 7) === '::ffff:') ip = ip.substr(7);
			if (ip === '0.0.0.0' || ip === '127.0.0.1' || ip == 'localhost') return resolve(true);

			const options: https.RequestOptions = {
        host: 'drukteradar.cybox.nl',
        path: '/api/getWhitelist/',
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': process.env.API_KEY
        }
      }

      // Opens the HTTP request, and sets the event listeners, once
      //  the request is complete, parse and check for address
      const request = https.request(options, (res: IncomingMessage) => {
        let body: string = '';

        res.on('error', (err: Error) => reject(err));
        res.on('data', (chunk: any) => body += chunk);
        res.on('close', () => {
          let parsed: any = JSON.parse(body);
          parsed.forEach((address: string) => {
            if (address === ip) resolve(true);
          });

          resolve(false);
        });
      });

      // Sets the timeout of the request, and adds the timeout
      //  listener, which closes the connection
      request.setTimeout(4000);
      request.on('timeout', () => request.destroy());

      // Writes the empty body of the packet
      request.write('');
      request.end();
    });
  };

  /**
   * Forwards an packet to to the endpoint
   * @param pkt the packet to be forwarded
   */
  public static forward = (pkt: CBXPKT): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      const options: https.RequestOptions = {
        host: 'drukteradar.cybox.nl',
        path: '/api/',
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': pkt.getBody().getApiKey().substring(0, 17) /* remove the '\0' */
        }
      };

      // Opens the HTTP request, and sets the event listeners, once
      //  the request is complete, check status and write error if not 201
      const request =  https.request(options, (res: IncomingMessage) => {
        let body: string = '';

        res.on('error', (err: Error) => reject(err));
        res.on('data', (chunk: any) => body += chunk);
        res.on('close', () => {
          if (res.statusCode != 201)
            reject(new Error(`${res.statusCode}: ${body}`))
          else resolve();
        });
      });
      
      // Sets the timeout of the request, and adds the timeout
      //  listener, which closes the connection
      request.setTimeout(4000);
      request.on('timeout', () => request.destroy());

      // Sets the error event
      request.on('error', (err: Error) => reject(err));

      // Writes the JSON version of the packet
      request.write(pkt.toJSON());
      request.end();
    });
  };
}
