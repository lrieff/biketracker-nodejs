import chalk from 'chalk';

export enum LoggerLevel {
  Debug, Info, Warn, Error, Fatal
};

export class Logger {
  public static s_MinLevel: LoggerLevel = LoggerLevel.Debug;

  private m_Prefix: string;
  private m_Level: LoggerLevel;

  /************************************
   * Public methods
   ***********************************/
  
  /**
   * Default constructor for Logger
   * @param prefix the prefix in front of each message
   * @param level the level of the current logger
   */
  public constructor(prefix: string, level: LoggerLevel) {
    this.m_Prefix = prefix;
    this.m_Level = level;
  }

  /************************************
   * Private methods
   ***********************************/

  /**
   * Logs with an temp level
   * @param level the temp level
   * @param args the stuff to log
   */
  public level_log = (level: LoggerLevel, ...args: any[]): void => {
    const old: LoggerLevel = this.m_Level;
    this.m_Level = level;
    this.log(...args);
    this.m_Level = old;
  };

  /**
   * Logs the values in args
   * @param args the values to be logged
   */
  public log = (...args: any[]): void => {
    const now = new Date();
    
    process.stdout.write(`${now.toLocaleDateString()} ${now.toLocaleTimeString()} -> `);
    process.stdout.write(chalk.red(`[${LoggerLevel[this.m_Level]}@${this.m_Prefix}]: `));
    args.forEach((arg: any, index: number) => process.stdout.write(arg));
    process.stdout.write('\r\n');
  };
};