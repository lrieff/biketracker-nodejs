import { Server } from './server/server';
import { Logger, LoggerLevel } from './logger';
import { Endpoint } from './endpoint';

/**
 * The main entry point for the application
 * @param argc number of arguments
 * @param argv arguments itself
 */
const main = (argc: number, argv: string[]): void => {
  const logger: Logger = new Logger('Main', LoggerLevel.Info);
	if (process.env.DEBUG === 'Y') logger.log('Ontwikkelings-modus is actief, whitelist wordt niet gebruikt ..');

  logger.level_log(LoggerLevel.Warn, 'Software mogelijk niet stabiel !');
  logger.log('Server wordt opgestart ...');

  let server: Server = new Server(8800, 8801);
  server.createServer()
  server.genKeys().then(() => server.listen());
};

// Calls main, and exits the program with the supplied code
main(process.argv.length, process.argv);
