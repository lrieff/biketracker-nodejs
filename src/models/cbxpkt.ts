import { Logger } from "../logger";

export class Measurement {
  private m_Mac: number[];
  private m_FirstSeen: number;
  private m_LastSeen: number;

  public constructor(mac: number[], firstSeen: number, lastSeen: number) {
    this.m_Mac = mac;
    this.m_FirstSeen = firstSeen;
    this.m_LastSeen = lastSeen;
  }

  public setMac = (mac: number[]): void => { this.m_Mac = mac; }
  public getMac = (): number[] => this.m_Mac;
  public getMacNumber = (): number => {
    let result: number = 0x0;

    result |= (this.m_Mac[0] << 0);
    result |= (this.m_Mac[1] << 8);
    result |= (this.m_Mac[2] << 16);
    result |= (this.m_Mac[3] << 24);
    result |= (this.m_Mac[4] << 32);
    result |= (this.m_Mac[5] << 40);

    return result;
  };

  public setFirstSeen = (s: number): void => { this.m_FirstSeen = s; }
  public getFirstSeen = (): number => this.m_FirstSeen;

  public setLastSeen = (l: number): void => { this.m_LastSeen = l; }
  public getLastSeen = (): number => this.m_LastSeen;
};

export class CBXPKT_Flags {
  private m_Encrypted: boolean;
  private m_Relayed: boolean;
  private m_Chained: boolean;

  public constructor() {
    this.m_Encrypted = false;
    this.m_Relayed = false;
    this.m_Chained = false;
  }

  public parse = (raw: number): void => {
    this.m_Encrypted = raw & (0x1 << 0) ? true : false;
    this.m_Relayed = raw & (0x1 << 1) ? true : false;
    this.m_Chained = raw & (0x1 << 2) ? true : false;
  }

  /************************************
   * Getters / Setters 
   ***********************************/

  public setEncrypted = (e: boolean): void => { this.m_Encrypted = e; }
  public getEncrypted = (): boolean => this.m_Encrypted;

  public setRelayed = (r: boolean): void => { this.m_Relayed = r; }
  public getRelayed = (): boolean => this.m_Relayed;

  public setChained = (c: boolean): void => { this.m_Chained = c; }
  public getChained = (): boolean => this.m_Chained;
};

export class CBXPKT_Hdr {
  private m_Label: string;
  private m_Sender: number[];
  private m_Receiver: number[];
  private m_ChainNo: number;
  private m_Flags: CBXPKT_Flags;
  
  /**
   * The default constructor for CBXPKT_Hdr
   */
  public constructor() {
    this.m_Label = '';
    this.m_Sender = [];
    this.m_Receiver = [];
    this.m_ChainNo = 0;
    this.m_Flags = new CBXPKT_Flags();
  }

  /**
   * Parses the header into the current instance
   * @param raw the raw buffer
   * @param offset the offset in the buffer
   */
  public parse = (raw: Buffer, offset: number): number => {
    // Reads the label from the header, and converts the numbers
    //  to actually valid chars
    for (let i: number = 0; i < 4; ++i) 
      this.m_Label += String.fromCharCode(raw.readUInt8(offset + i));
    offset += 4;

    // Reads the sender mac from the raw header
    for (let i: number = 0; i < 6; ++i)
      this.m_Sender[i] = raw.readUInt8(offset + i);
    offset += 6;

    // Reads the receiver mac from the raw header
    for (let i: number = 0; i < 6; ++i)
      this.m_Receiver[i] = raw.readUInt8(offset + i);
    offset += 6;

    // Reads the chain number fro mthe header, and parses
    //  the flags into booleans
    this.m_ChainNo = raw.readUInt8(offset++);
    this.m_Flags.parse(raw.readUInt8(offset++));
    
    return offset;
  };
};

export class CBXPKT_Body {
  private m_UniqueID: number;
  private m_ApiKey: string;
  private m_Size: number;
  private m_Payload: Measurement[];

  /**
   * Default constructor for CBXPKT_Body
   */
  public constructor() {
    this.m_UniqueID = this.m_Size = 0;
    this.m_ApiKey = '';
    this.m_Payload = [];
  }

  /**
   * Parses an raw body into current instance
   * @param raw the raw buffer
   * @param offset the offset into the buffer
   */
  public parse = (raw: Buffer, offset: number): number => {
    // Reads the unique message id from the body, this is used
    //  to prevent copy's across gateways
    this.m_UniqueID = raw.readUInt32LE(offset);
    offset += 4;

    // Reads the api key, which later is used in the HTTP forwarding
    for (let i: number = 0; i < 18; ++i)
      this.m_ApiKey += String.fromCharCode(raw.readUInt8(offset + i));
    offset += 18;

    // Reads the size
    this.m_Size = raw.readUInt8(offset++);

    // Parses the payload / measurements from the body
    for (let i = 0; i < this.m_Size; i += 6) {
      this.m_Payload.push(new Measurement([
        raw.readUInt8(offset + 0), raw.readUInt8(offset + 1),
        raw.readUInt8(offset + 2), raw.readUInt8(offset + 3),
        raw.readUInt8(offset + 4), raw.readUInt8(offset + 5)
      ], Date.now() / 1000, Date.now() / 1000));
      offset += 6;
    }

    return offset;
  }

  /************************************
   * Getters / Setters
   ***********************************/

  public getPayload = (): Measurement[] => this.m_Payload;
  
  public getApiKey = (): string => this.m_ApiKey;

  public getUniqueID = (): number => this.m_UniqueID;
  
  public getSize = (): number => this.m_Size;
}

export class CBXPKT {
  private m_Hdr: CBXPKT_Hdr;
  private m_Body: CBXPKT_Body;

  /**
   * Default constructor for CBXPKT
   */
  public constructor() {
    this.m_Hdr = new CBXPKT_Hdr();
    this.m_Body = new CBXPKT_Body();
  };

  /**
   * Parses an CBXPKT into the current instance
   * @param raw the raw buffer
   * @param offset the current offset in the buffer
   */
  public parse = (raw: Buffer, offset: number): number => {
    offset = this.m_Hdr.parse(raw, offset);
    offset = this.m_Body.parse(raw, offset);

    return offset;
  };

  /**
   * Returns the JSON version of an CBXPKT
   */
  public toJSON = (): string => {
    // Builds the base object which contains the current date in seconds
    //  and the number[][] of the payload
    let obj: {
      ts: number,
      d: number[][]
    } = {
      ts: Date.now() / 1000,
      d: []
    };

    // Adds the payload, actually an array of arrays, which is in the
    //  following format: [ mac, first, last ]
    this.m_Body.getPayload().forEach(m => {
      obj.d.push([
        m.getMacNumber(), m.getFirstSeen(), m.getLastSeen()
      ]);
    });

    // Returns the string version of the object
    return JSON.stringify(obj);
  };

  /************************************
   * Getters / Setters
   ***********************************/

  public getHdr = (): CBXPKT_Hdr => this.m_Hdr;

  public getBody = (): CBXPKT_Body => this.m_Body;
};