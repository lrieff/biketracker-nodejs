import tls from 'tls';
import net from 'net';
import pem from 'pem';

import { Logger, LoggerLevel } from '../logger';
import { CBXPKT } from '../models/cbxpkt';
import { Endpoint } from '../endpoint';

export class Server {
  public static s_BacklogSize: number = 300;

  private m_Logger: Logger;
  private m_TLSServer?: tls.Server;
  private m_Server?: net.Server;
  private m_TLSPort: number;
  private m_Port: number;
  private m_Hostname: string = '0.0.0.0';
  private m_PrivateKey?: string;
  private m_Cert?: string;

  /************************************
   * Public methods
   ***********************************/

   /**
    * Default constructor for the Server
    * @param tlsPort the port for the TLS server
    * @param port the port for the plain server
    * @param hostname the hostname to listen on
    */
  public constructor(tlsPort: number, port: number, hostname = '0.0.0.0') {
    this.m_TLSPort = tlsPort;
    this.m_Port = port;
    this.m_Hostname = hostname;

    this.m_Logger = new Logger('Server', LoggerLevel.Debug);
  }
  
  /**
   * Creates the TLS server instance
   */
  public createServer = (): void => {
    this.m_TLSServer = tls.createServer({
      key: this.m_PrivateKey,
      cert: this.m_Cert
    }, this.handler);

    this.m_Server = net.createServer(this.handler);
  }

  /**
   * Generates the self signed certificate and private key
   */
  public genKeys = (): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      pem.createCertificate({
        days: 365,
        selfSigned: true
      }, (error: any, result: pem.CertificateCreationResult) => {
        if (error) reject(error);
       
        this.m_PrivateKey = result.serviceKey;
        this.m_Cert = result.certificate;
  
        resolve();
      });
    });
  };

  /**
   * Listens the server
   */
  public listen = (): void => {
    if (this.m_TLSServer) {
      this.m_TLSServer.listen(this.m_TLSPort, this.m_Hostname, Server.s_BacklogSize, () => {
        this.m_Logger.log(`TLS Server listening on: ${this.m_Hostname}:${this.m_TLSPort}`)
      });

      this.m_TLSServer.on('tlsClientError', (err: Error, socket: tls.TLSSocket) => {
        this.m_Logger.level_log(LoggerLevel.Error, `TLS Server error: `, 
          err.message);
      });
  }

  if (this.m_Server) {
    this.m_Server.listen(this.m_Port, this.m_Hostname, Server.s_BacklogSize, () => {
      this.m_Logger.log(`Server listening on: ${this.m_Hostname}:${this.m_Port}`)
    });

    this.m_Server.on('error', (err: Error, socket: tls.TLSSocket) => {
      this.m_Logger.level_log(LoggerLevel.Error, `Server error: `, 
        err.message);
    });
  }
}

  /************************************
   * Private methods
   ***********************************/

  /**
   * Method which gets called on a new connection
   * @param socket the client socket
   */
  private handler = (socket: net.Socket): void  => {    
    const logger: Logger = new Logger(`Server:${socket.remoteAddress}`, LoggerLevel.Debug);
    logger.log(`Nieuwe verbinding op port: ${socket.remotePort}`);

    // Sets the socket timeout to prevent unused sockets
    socket.setTimeout(60 * 60 * 1000);

    // Checks if the client is listed
    Endpoint.checkWhitelist(socket.remoteAddress ? socket.remoteAddress : 'invalid').then(listed => {
      if (!listed) {
        logger.log(`Client not listed`);
        socket.destroy();
        return;
      }
    })

    // Sets the data callback
    let buffer: string = '';
    socket.on('data', (response: Buffer) => {
      buffer += response.toString('utf-8');
      console.log(response.toString('utf-8'));

      // Checks if the buffer data contains an newline, if that is the case
      //  decode the message, and parse it
      if (buffer.length > 1 && buffer.charAt(buffer.length - 1) == '\n') {
        const decoded: Buffer = Buffer.from(buffer, 'hex');

        // Attempts to parse the packet
        try {
          const pkt: CBXPKT = new CBXPKT();
          pkt.parse(decoded, 0)
          console.log(pkt);

          const start: number = Date.now();
          Endpoint.forward(pkt).then(() => {
            logger.log(`Packet doorgestuurd in ${Date.now() - start}ms`);
          }).catch(err => {
            logger.level_log(LoggerLevel.Error, 'Doorsturen pakket is gefaald: ', err.message);
          });
        } catch (e) {
          logger.level_log(LoggerLevel.Error, 'Fout bij parsen van pakket: ', e.message);
        }

        buffer = '';
      }
    });

    // Sets the timeout callback
    socket.on('timeout', () => {
      logger.level_log(LoggerLevel.Warn, 'Socket te lang inactief, verbinding sluiten ...');
      socket.destroy()
    });

    // Sets the close callback
    socket.on('close', () => {
      logger.level_log(LoggerLevel.Warn, 'Verbinding gesloten !')
    });
  }
};